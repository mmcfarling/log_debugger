# README

This project is built with Rails 5.1.6.2 using Ruby 2.5.1 (ruby 2.5.1p57 (2018-03-29 revision 63029) [x86_64-darwin17]).  It uses the seen_malware.sqlite3 database as its source.  The seen_malware.sqlite3 database file should be located in the ```db/``` folder of the project.

Run ```rails s``` to start the project.  Also make sure that Redis is running ```redis-server``` and that sidekiq is processing jobs ```bundle exec sidekiq```.

![alt text](public/screenshot.png "Connector Log App")
