Rails.application.routes.draw do
  root 'debugger#index'

  post '/log-entries', to: 'debugger#analyze'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
