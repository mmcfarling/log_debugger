# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ -> 
	$('.file-name').hide()
	$('input[type=file]').change (e) ->
  	$('.file-name').html e.target.files[0].name
  	$('.file-name').show()
  return