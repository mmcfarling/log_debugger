class DebuggerController < ApplicationController
  def analyze
  	filename = params[:log_entries].tempfile
  	LogEntriesJob.perform_later(filename.path)
  end
end
