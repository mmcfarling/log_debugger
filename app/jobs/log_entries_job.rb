class LogEntriesJob < ApplicationJob
  queue_as :default
  
  def perform(*args)
  	filename = args[0]
  	
  	File.foreach(filename) do |log_line|
  		parsed_log_line = JSON.parse(log_line)
  		if parsed_log_line['dp'] == 3
  			begin
					malware = SeenMalware.find(parsed_log_line['sha'])
					malware.update_attribute('cnt', malware.cnt + 1)
				rescue ActiveRecord::RecordNotFound
					SeenMalware.create(sha: parsed_log_line['sha'], cnt: 1, dp: 3)
				end
  		end
  	end
  end
end