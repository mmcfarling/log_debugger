class CreateLogRuns < ActiveRecord::Migration[5.1]
  def change
    create_table :log_runs do |t|
      t.string :jid
      t.integer :seen
      t.integer :unseen
      t.integer :processed_lines

      t.timestamps
    end
  end
end
